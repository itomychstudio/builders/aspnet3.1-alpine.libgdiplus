# aspnet3.1-alpine.libgdiplus 

This project builds a docker image with basic tools for .NET projects based on Docker and System.Drawing.

## Usage
Ex. Inside your Dockerfile.Alpine file make the following changes:

```
FROM registry.gitlab.com/itomychstudio/builders/aspnet3.1-alpine.libgdiplus:latest AS base
WORKDIR /app
EXPOSE 80

COPY ./bin/Release/netcoreapp3.1/publish .
ENTRYPOINT ["dotnet", "MyBest.Api.dll"]

```


## Contributing
Create branch with the 'feature/ticket-name' name. Make changes and commit.
Then create the merge request into 'develop' branch.

***
## Authors and acknowledgment
ITOMYCH STUDIO/DashDevs LLC
